/*

    test0, Foward Relational Integrity Tests:
        if the joined foreign key retains all original records, test passes
    test1, Backward Relational Integrity Tests:
        if the joined foreign key retains a certain amount of referenced primary key, test passes
        Will use 25% for the exercise.
        
*/

-- test0, Foward Relational Integrity Tests
set @PRESCRIBING_DEMOGRAPHIC_test0 := (select case
	when count_res = (select count(PATID) from PRESCRIBING) then 'OK'
    else 'FAIL'
end from (
	select count(L.PATID) as count_res
	from PRESCRIBING L
	inner join DEMOGRAPHIC R
	on L.PATID = R.PATID
) count_agg);


set @PRESCRIBING_ENCOUNTER_test0 := (select case
	when count_res = (select count(ENCOUNTERID) from PRESCRIBING) then 'OK'
    else 'FAIL'
end from (
	select count(L.ENCOUNTERID) as count_res
	from PRESCRIBING L
	inner join ENCOUNTER R
	on L.ENCOUNTERID = R.ENCOUNTERID
) count_agg);


set @PRESCRIBING_PROVIDER_test0 := (select case
	when count_res = (select count(RX_PROVIDERID) from PRESCRIBING) then 'OK'
    else 'FAIL'
end from (
	select count(L.RX_PROVIDERID) as count_res
	from PRESCRIBING L
	inner join PROVIDER R
	on L.RX_PROVIDERID = R.PROVIDERID
) count_agg);


-- test1, Backward Relational Integrity Tests
set @DEMOGRAPHIC_PRESCRIBING_test1 := (select case
	when count_res / (select count(PATID) from DEMOGRAPHIC) > 0.25 then 'OK'
    else 'FAIL'
end from (
	select count(distinct R.PATID) as count_res
	from DEMOGRAPHIC R
	inner join PRESCRIBING L
	on R.PATID = L.PATID
) count_agg);


set @ENCOUNTER_PRESCRIBING_test1 := (select case
	when count_res / (select count(ENCOUNTERID) from ENCOUNTER) > 0.25 then 'OK'
    else 'FAIL'
end from (
	select count(distinct R.ENCOUNTERID) as count_res
	from ENCOUNTER R
	inner join PRESCRIBING L
	on R.ENCOUNTERID = L.ENCOUNTERID
) count_agg);


set @PROVIDER_PRESCRIBING_test1 := (select case
	when count_res / (select count(PROVIDERID) from PROVIDER) > 0.25 then 'OK'
    else 'FAIL'
end from (
	select count(distinct R.PROVIDERID) as count_res
	from PROVIDER R
	inner join PRESCRIBING L
	on R.PROVIDERID = L.RX_PROVIDERID
) count_agg);


-- Test Results' Summary
select 'test0' as TEST, @PRESCRIBING_DEMOGRAPHIC_test0 as RESULT UNION ALL
select 'test0' as TEST, @PRESCRIBING_ENCOUNTER_test0 as RESULT UNION ALL
select 'test0' as TEST, @PRESCRIBING_PROVIDER_test0 as RESULT UNION ALL
select 'test1' as TEST, @DEMOGRAPHIC_PRESCRIBING_test1 as RESULT UNION ALL
select 'test1' as TEST, @ENCOUNTER_PRESCRIBING_test1 as RESULT UNION ALL
select 'test1' as TEST, @PROVIDER_PRESCRIBING_test1 as RESULT
;