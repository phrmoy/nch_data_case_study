/*

    test0, Uniqueness Constraint Tests:
        If the max field-aggregated count is one, test passes
    test1, Not NULL Constraint Tests:
        If the total count of records where field is NULL zero, test passes
        
*/

-- test0, Uniqueness Constraint Tests
set @PRESCRIBING_PRESCRIBINGID_test0 := (select case
	when max_res = 1 then 'OK'
	else 'FAIL'
end from (
	select max(count_res) as max_res
	from (
		select PRESCRIBINGID,count(PRESCRIBINGID) as count_res
		from PRESCRIBING
		group by PRESCRIBINGID
	) count_agg
) max_agg);


set @DEMOGRAPHIC_PATID_test0 := (select case
	when max_res = 1 then 'OK'
	else 'FAIL'
end from (
	select max(count_res) as max_res
	from (
		select PATID,count(PATID) as count_res
		from DEMOGRAPHIC
		group by PATID
	) count_agg
) max_agg);


set @ENCOUNTER_ENCOUNTERID_test0 := (select case
	when max_res = 1 then 'OK'
	else 'FAIL'
end from (
	select max(count_res) as max_res
	from (
		select ENCOUNTERID,count(ENCOUNTERID) as count_res
		from ENCOUNTER
		group by ENCOUNTERID
	) count_agg
) max_agg);


set @PROVIDER_PROVIDERID_test0 := (select case
	when max_res = 1 then 'OK'
	else 'FAIL'
end from (
	select max(count_res) as max_res
	from (
		select PROVIDERID,count(PROVIDERID) as count_res
		from PROVIDER
		group by PROVIDERID
	) count_agg
) max_agg);


-- test1, Not NULL Constraint Tests
set @PRESCRIBING_PATID_test1 := (select case
	when count_res = 0 then 'OK'
	else 'FAIL'
end from (
	select count(PATID) as count_res
	from PRESCRIBING
	where PATID is NULL
	) count_agg
);


set @PRESCRIBING_ENCOUNTERID_test1 := (select case
	when count_res = 0 then 'OK'
	else 'FAIL'
end from (
	select count(ENCOUNTERID) as count_res
	from PRESCRIBING
	where ENCOUNTERID is NULL
	) count_agg
);


set @PRESCRIBING_RX_PROVIDERID_test1 := (select case
	when count_res = 0 then 'OK'
	else 'FAIL'
end from (
	select count(RX_PROVIDERID) as count_res
	from PRESCRIBING
	where RX_PROVIDERID is NULL
	) count_agg
);


-- Test Results' Summary
select 'test0' as TEST, @PRESCRIBING_PRESCRIBINGID_test0 as RESULT UNION ALL
select 'test0' as TEST, @DEMOGRAPHIC_PATID_test0 as RESULT UNION ALL
select 'test0' as TEST, @ENCOUNTER_ENCOUNTERID_test0 as RESULT UNION ALL
select 'test0' as TEST, @PROVIDER_PROVIDERID_test0 as RESULT UNION ALL
select 'test1' as TEST, @PRESCRIBING_PATID_test1 as RESULT UNION ALL
select 'test1' as TEST, @PRESCRIBING_ENCOUNTERID_test1 as RESULT UNION ALL
select 'test1' as TEST, @PRESCRIBING_RX_PROVIDERID_test1 as RESULT
;