/*

    ValueSet Conformity Test:
        If target field is in pre-specified valueset, test passes
        
*/

-- test0, Uniqueness Constraint Tests
set @PRESCRIBING_RX_DOSE_ORDERED_UNIT := (select case
when count_res = (select count(RX_DOSE_ORDERED_UNIT)
					from PRESCRIBING
					where RX_DOSE_ORDERED_UNIT is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_DOSE_ORDERED_UNIT) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_DOSE_ORDERED_UNIT R
	on L.RX_DOSE_ORDERED_UNIT = R.RX_DOSE_ORDERED_UNIT
	where L.RX_DOSE_ORDERED_UNIT is not NULL
) count_agg);


set @PRESCRIBING_RX_DOSE_FORM := (select case
when count_res = (select count(RX_DOSE_FORM)
					from PRESCRIBING
					where RX_DOSE_FORM is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_DOSE_FORM) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_DOSE_FORM R
	on L.RX_DOSE_FORM = R.RX_DOSE_FORM
	where L.RX_DOSE_FORM is not NULL
) count_agg);


set @PRESCRIBING_RX_FREQUENCY := (select case
when count_res = (select count(RX_FREQUENCY)
					from PRESCRIBING
					where RX_FREQUENCY is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_FREQUENCY) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_FREQUENCY R
	on L.RX_FREQUENCY = R.RX_FREQUENCY
	where L.RX_FREQUENCY is not NULL
) count_agg);


set @PRESCRIBING_RX_PRN_FLAG := (select case
when count_res = (select count(RX_PRN_FLAG)
					from PRESCRIBING
					where RX_PRN_FLAG is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_PRN_FLAG) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_PRN_FLAG R
	on L.RX_PRN_FLAG = R.RX_PRN_FLAG
	where L.RX_PRN_FLAG is not NULL
) count_agg);


set @PRESCRIBING_RX_ROUTE := (select case
when count_res = (select count(RX_ROUTE)
					from PRESCRIBING
					where RX_ROUTE is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_ROUTE) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_ROUTE R
	on L.RX_ROUTE = R.RX_ROUTE
	where L.RX_ROUTE is not NULL
) count_agg);


set @PRESCRIBING_RX_BASIS := (select case
when count_res = (select count(RX_BASIS)
					from PRESCRIBING
					where RX_BASIS is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_BASIS) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_BASIS R
	on L.RX_BASIS = R.RX_BASIS
	where L.RX_BASIS is not NULL
) count_agg);


set @PRESCRIBING_RX_SOURCE := (select case
when count_res = (select count(RX_SOURCE)
					from PRESCRIBING
					where RX_SOURCE is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_SOURCE) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_SOURCE R
	on L.RX_SOURCE = R.RX_SOURCE
	where L.RX_SOURCE is not NULL
) count_agg);


set @PRESCRIBING_RX_DISPENSE_AS_WRITTEN := (select case
when count_res = (select count(RX_DISPENSE_AS_WRITTEN)
					from PRESCRIBING
					where RX_DISPENSE_AS_WRITTEN is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.RX_DISPENSE_AS_WRITTEN) as count_res
	from PRESCRIBING L
	inner join VS_PRESCRIBING_RX_DISPENSE_AS_WRITTEN R
	on L.RX_DISPENSE_AS_WRITTEN = R.RX_DISPENSE_AS_WRITTEN
	where L.RX_DISPENSE_AS_WRITTEN is not NULL
) count_agg);


-- Test Results' Summary
select 'RX_DOSE_ORDERED_UNIT' as TEST, @PRESCRIBING_RX_DOSE_ORDERED_UNIT as RESULT UNION ALL
select 'RX_DOSE_FORM' as TEST, @PRESCRIBING_RX_DOSE_FORM as RESULT UNION ALL
select 'RX_FREQUENCY' as TEST, @PRESCRIBING_RX_FREQUENCY as RESULT UNION ALL
select 'RX_PRN_FLAG' as TEST, @PRESCRIBING_RX_PRN_FLAG as RESULT UNION ALL
select 'RX_ROUTE' as TEST, @PRESCRIBING_RX_ROUTE as RESULT UNION ALL
select 'RX_BASIS' as TEST, @PRESCRIBING_RX_BASIS as RESULT UNION ALL
select 'RX_SOURCE' as TEST, @PRESCRIBING_RX_SOURCE as RESULT UNION ALL
select 'RX_DISPENSE_AS_WRITTEN' as TEST, @PRESCRIBING_RX_DISPENSE_AS_WRITTEN as RESULT
;