"""
This module parses PCORnet-CMD-4.1-parsable to obtain the schema metadata.

Discrepancies with PCORnet-CMD-4.1v-parsable
1) Table names are not consitent with underscores
    Solution: Removed all underscores from table names
2) Field MEDADMIN_END_DATE_MGMT is missing from FIELDS
    Solution: Ignored.

* These solutions are applied among different scripts
"""

from utils import get_meta_path,cache,load_cache
from relations_parser import *
import pandas as pd

class Constraints:
    def __init__(self,data):
        self.data = {}
        for _,d in data.iterrows():
            key = [i.strip() for i in d["FIELD_NAME"].split("+")]
            if len(key) > 1:
                key = tuple(key)
            else:
                key = key[0]
            val = d["CONSTRAINT"].split(",")[-1]
            val = "_".join(val.split()).lower()
            if key in self.data:
                self.data[key].add(val)
            else:
                tmp = set()
                tmp.add(val)
                self.data[key] = tmp

class Field:
    def __init__(self,data):
        self.field = data["FIELD_NAME"].strip()
        self.type = data["RDBMS_DATA_TYPE"].replace("RDBMS","").replace("RBDMS","").strip()

class ValueSet:
    def __init__(self,data):
        if sum(data["VALUESET_ITEM"].isnull()) > 0:
            # print "WTF1",data["VALUESET_ITEM_DESCRIPTOR"]
            self.values = set([str(d).split("=")[0].encode('utf-8').strip().replace("'","\\'") for d in data["VALUESET_ITEM_DESCRIPTOR"]])
        else:
            # print "WTF2",data["VALUESET_ITEM"]
            self.values = set([str(d).encode('utf-8').strip().replace("'","\\'") for d in data["VALUESET_ITEM"]])



class MetaData:
    build_cache = "build.pickle"
    def __init__(self):
        pass
    def build(self):
        res = load_cache(self.build_cache)
        if res is None:
            print "Building Meta.."
            filep = get_meta_path()
            sheets = ["RELATIONAL","CONSTRAINTS","FIELDS","VALUESETS"]
            for sheet in sheets:
                data = pd.read_excel(filep,sheet)
                self.fix_table_name(data)
                setattr(self,sheet,data)
            cache(self,self.build_cache)
            return self
        else:
            return res
    def fix_table_name(self,ob):
        ob["TABLE_NAME"] = ob["TABLE_NAME"].str.strip().str.replace("_","").str.replace("PCORNETTRIAL","PCORNET")
    def get_relations(self):
        res = {}
        for table,data in self.RELATIONAL.groupby("TABLE_NAME"):
            res[table] = Relations(data)
        return res
    def get_constraints(self):
        res = {}
        for table,data in self.CONSTRAINTS.groupby("TABLE_NAME"):
            res[table] = Constraints(data)
        return res
    def get_fields(self):
        res = {}
        for table,data in self.FIELDS.groupby("TABLE_NAME"):
            res[table] = [Field(d) for _,d in data.iterrows()]
        return res
    def get_valuesets(self):
        res = {}
        for table,data in self.VALUESETS.groupby("TABLE_NAME"):
            data = data.groupby("FIELD_NAME")
            res[table] = { key: ValueSet(d) for key,d in data}
        return res

if __name__ == "__main__":
    meta = MetaData().build()
    # print meta.get_relations()
    # print meta.get_constraints()
    # print meta.get_fields()
    meta.get_valuesets()
