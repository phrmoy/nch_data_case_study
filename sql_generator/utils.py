"""
This module contains simple utilities for scripting.
"""

import os
import cPickle

META_DIR = "meta"
META_FILE = "2018-05-29-PCORnet-Common-Data-Model-v4dot1-parseable.xlsx"
SCRIPTS_DIR = "sql_scripts"

def get_project_dir():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def get_meta_dirn():
    return os.path.join(get_project_dir(),META_DIR)

def get_meta_path():
    return os.path.join(get_meta_dirn(),META_FILE)

def get_sql_scripts_dir():
    return os.path.join(get_project_dir(),SCRIPTS_DIR)

def cache(data,filen):
    with open(os.path.join(get_meta_dirn(),filen),"wb") as f:
        cPickle.dump(data,f)

def load_cache(filen):
    cache_filep = os.path.join(get_meta_dirn(),filen)
    if os.path.isfile(cache_filep):
        with open(cache_filep,"rb") as f:
            return cPickle.load(f)
    else:
        return None


if __name__ == "__main__":
    assert os.path.isdir(get_project_dir())
    assert os.path.isdir(get_meta_dirn())
    assert os.path.isfile(get_meta_path())
