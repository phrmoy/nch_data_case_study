"""
This module provides a base class for classes generating SQL scripts.
"""

from utils import get_sql_scripts_dir
import os
import re

SEP = os.linesep

def map_type(xs):
    if xs == "Number(x)":
        return "DECIMAL(65,30)"
    elif xs == "Date":
        return "DATE"
    elif re.search(r"Text.*\((.+)\)",xs) is not None:
        x = re.search(r"Text.*\((.+)\)",xs).group(1)
        if x == "x":
            return "VARCHAR(256)"
        else:
            return "CHAR({0})".format(x)
    else:
        raise Exception("Did not recognize type.")

class QueryBuilder(object):
    sql_script_name = None
    def __init__(self):
        self.query = []
    def append(self,query):
        self.query.append(query)
    def break_line(self):
        self.append("")
    def title(self,text):
        self.break_line()
        self.append("-- "+text)
    def write_query(self):
        with open(os.path.join(get_sql_scripts_dir(),self.sql_script_name),"wb") as f:
            f.write(SEP.join(self.query))

if __name__ == "__main__":
    pass