"""
"""

from sql_generator import QueryBuilder,SEP
import os

def uniqueness_test(q,table,field):
    template = """set @{table}_{field}_test0 := (select case
	when max_res = 1 then 'OK'
	else 'FAIL'
end from (
	select max(count_res) as max_res
	from (
		select {field},count({field}) as count_res
		from {table}
		group by {field}
	) count_agg
) max_agg);
""".format(table=table,field=field)
    q.append(template)
    q.break_line()

def nullability_test(q,table,field):
    template = """set @{table}_{field}_test1 := (select case
	when count_res = 0 then 'OK'
	else 'FAIL'
end from (
	select count({field}) as count_res
	from {table}
	where {field} is NULL
	) count_agg
);
""".format(table=table,field=field)
    q.append(template)
    q.break_line()

def test_result(acc,table,field,test):
    template = "select '{test}' as TEST, @{table}_{field}_{test} as RESULT".format(table=table,field=field,test=test)
    acc.append(template)

if __name__ == "__main__":
    q = QueryBuilder()

    q.append("""/*

    test0, Uniqueness Constraint Tests:
        If the max field-aggregated count is one, test passes
    test1, Not NULL Constraint Tests:
        If the total count of records where field is NULL zero, test passes
        
*/""")

    q.break_line()
    q.sql_script_name = os.path.basename(__file__).replace(".py",".sql")
    uniqueness_test_set = [
        ("PRESCRIBING","PRESCRIBINGID"),
        ("DEMOGRAPHIC","PATID"),
        ("ENCOUNTER","ENCOUNTERID"),
        ("PROVIDER","PROVIDERID")
    ]
    nullability_test_set = [
        ("PRESCRIBING","PATID"),
        ("PRESCRIBING","ENCOUNTERID"),
        ("PRESCRIBING","RX_PROVIDERID"),
    ]

    q.append("-- test0, Uniqueness Constraint Tests")
    for (table,field) in uniqueness_test_set:
        uniqueness_test(q,table,field)

    q.append("-- test1, Not NULL Constraint Tests")
    for (table,field) in nullability_test_set:
        nullability_test(q,table,field)

    q.append("-- Test Results' Summary")
    tmp = []
    for (table,field) in uniqueness_test_set:
        test_result(tmp,table,field,"test0")
    for (table,field) in nullability_test_set:
        test_result(tmp,table,field,"test1")
    q.append(" UNION ALL{0}".format(SEP).join(tmp))
    q.append(";")

    q.write_query()