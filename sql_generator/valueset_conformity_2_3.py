"""
"""

from sql_generator import QueryBuilder,SEP
import os

def test(q,table,field):
    template = """set @{table}_{field} := (select case
when count_res = (select count({field})
					from {table}
					where {field} is not NULL)
then 'OK'
else 'FAIL'
end
from (
	select count(L.{field}) as count_res
	from {table} L
	inner join VS_{table}_{field} R
	on L.{field} = R.{field}
	where L.{field} is not NULL
) count_agg);
""".format(table=table,field=field)
    q.append(template)
    q.break_line()

def test_result(acc,table,field):
    template = "select '{field}' as TEST, @{table}_{field} as RESULT".format(table=table,field=field,test=test)
    acc.append(template)

if __name__ == "__main__":
    q = QueryBuilder()

    q.append("""/*

    ValueSet Conformity Test:
        If target field is in pre-specified valueset, test passes
        
*/""")

    q.break_line()
    q.sql_script_name = os.path.basename(__file__).replace(".py",".sql")
    test_set = """
        RX_DOSE_ORDERED_UNIT 
        RX_DOSE_FORM 
        RX_FREQUENCY 
        RX_PRN_FLAG 
        RX_ROUTE 
        RX_BASIS 
        RX_SOURCE
        RX_DISPENSE_AS_WRITTEN
    """
    table = "PRESCRIBING"
    test_set = test_set.split()

    q.append("-- test0, Uniqueness Constraint Tests")
    for field in test_set:
        test(q,table,field)

    q.append("-- Test Results' Summary")
    tmp = []
    for field in test_set:
        test_result(tmp,table,field)
    q.append(" UNION ALL{0}".format(SEP).join(tmp))
    q.append(";")

    q.write_query()