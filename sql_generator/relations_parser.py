"""
"""

import re

class PK:
    def __init__(self,data):
        assert data["RELATION"].strip() == "PK"
        self.field = data["RELATIONAL_INTEGRITY_DETAILS"].strip()
    def get_string(self):
        return "(`{0}`)".format(self.field)
    def assert_correct_fields(self,field_set):
        assert self.field in field_set
    def get_fields(self):
        return self.field

class CK:
    def __init__(self,data):
        assert data["RELATION"].strip() == "Composite Key"
        self.fields = tuple(map(lambda xs: xs.strip(),data["RELATIONAL_INTEGRITY_DETAILS"].split("+")))
    def get_string(self):
        return "({0})".format(",".join(["`{0}`".format(f) for f in self.fields]))
    def assert_correct_fields(self,field_set):
        assert all(f in field_set for f in self.fields)
    def get_fields(self):
        return self.fields

def get_pk(data):
    val = data["RELATION"].strip()
    if val == "PK":
        return PK(data)
    elif val == "Composite Key":
        return CK(data)
    else:
        raise Exception("Did not recognize primary key type.")

class FK:
    def __init__(self,data):
        """
        Corrections to field names are made here.
        """
        assert "FK" in data["RELATION"]
        fields = map(lambda xs: xs.strip(),data["RELATIONAL_INTEGRITY_DETAILS"].split("references"))
        self.table,self.field = fields[0].split(".")
        self.table,self.field = self.table.strip().replace("_",""),self.field.strip()

        if (self.table == "OBSGEN" or self.table == "OBSCLIN") and self.field == "PROVIDERID":
            self.field = self.table + "_" + self.field

        self.reference_table,self.reference_field=fields[1].split(".")
        self.reference_table, self.reference_field = self.reference_table.strip().replace("_",""),self.reference_field.strip()
        self.relation = re.search(r"\((.+)\)",data["RELATION"]).group(1)
    def assert_correct_fields(self,fields_sets):
        assert self.field in fields_sets[self.table] and self.reference_field in fields_sets[self.reference_table]

class Relations:
    def __init__(self,data):
        self.pk = get_pk(data.iloc[0,:])
        self.fks = [FK(data.iloc[i,:]) for i in range(1,data.shape[0])]

if __name__ == "__main__":
    pass