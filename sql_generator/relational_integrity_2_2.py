"""
"""

from sql_generator import QueryBuilder,SEP
import os

def foward_test(q,left_table,left_field,right_table,right_field):
    template = """set @{left_table}_{right_table}_test0 := (select case
	when count_res = (select count({left_field}) from {left_table}) then 'OK'
    else 'FAIL'
end from (
	select count(L.{left_field}) as count_res
	from {left_table} L
	inner join {right_table} R
	on L.{left_field} = R.{right_field}
) count_agg);
""".format(
        left_table = left_table,
        left_field = left_field,
        right_table = right_table,
        right_field = right_field
    )
    q.append(template)
    q.break_line()

def backward_test(q,left_table,left_field,right_table,right_field):
    template = """set @{right_table}_{left_table}_test1 := (select case
	when count_res / (select count({right_field}) from {right_table}) > 0.25 then 'OK'
    else 'FAIL'
end from (
	select count(distinct R.{right_field}) as count_res
	from {right_table} R
	inner join {left_table} L
	on R.{right_field} = L.{left_field}
) count_agg);
""".format(
        left_table = left_table,
        left_field = left_field,
        right_table = right_table,
        right_field = right_field
    )
    q.append(template)
    q.break_line()

def test_result(acc,left,right,test):
    template = "select '{test}' as TEST, @{left}_{right}_{test} as RESULT".format(left=left,right=right,test=test)
    acc.append(template)

if __name__ == "__main__":
    q = QueryBuilder()

    q.append("""/*

    test0, Foward Relational Integrity Tests:
        if the joined foreign key retains all original records, test passes
    test1, Backward Relational Integrity Tests:
        if the joined foreign key retains a certain amount of referenced primary key, test passes
        Will use 25% for the exercise.
        
*/""")

    q.break_line()
    q.sql_script_name = os.path.basename(__file__).replace(".py",".sql")
    test_set = [
        [("PRESCRIBING","PATID"),("DEMOGRAPHIC","PATID")],
        [("PRESCRIBING","ENCOUNTERID"),("ENCOUNTER","ENCOUNTERID")],
        [("PRESCRIBING","RX_PROVIDERID"),("PROVIDER","PROVIDERID")]
    ]

    q.append("-- test0, Foward Relational Integrity Tests")
    for (left,right) in test_set:
        foward_test(q,left[0],left[1],right[0],right[1])
    q.append("-- test1, Backward Relational Integrity Tests")
    for (left,right) in test_set:
        backward_test(q,left[0],left[1],right[0],right[1])

    q.append("-- Test Results' Summary")
    tmp = []
    for (left,right) in test_set:
        test_result(tmp,left[0],right[0],"test0")
    for (left,right) in test_set:
        test_result(tmp,right[0],left[0],"test1")
    q.append(" UNION ALL{0}".format(SEP).join(tmp))
    q.append(";")

    q.write_query()